# Hackaton DrupalCamp SP 2017

## Tutor ChatBot



Neste site você pode perguntar para o chatbot como fazer coisas no drupal 8 e o chatbot vai te ajudar na sua tarefa. Use o chat na sidebar para iniciar a conversar com o chatbot.

No futuro você vai poder conversar com o bot por voz e receber de respostas enquanto você trabalha no site.

Para configurar este mesmo site na sua máquina local, você pode clonar o codigo no github: https://github.com/wranvaud/tutorchatbot

Caso tenha o Drupal Console instalado na máquina, basta executar "drupal server" para iniciar o site. Um link será exibido no terminal. O banco de dados é sqlite e está incluido no repositório.
Passos detalhados para criar do zero

    Criar um site drupal 8
    Instale os modulos chatbot_api e api_ai_webhook
    Crie uma conta no Dialogflow e adicione Entities e Intents (ver documentação do dialogflow)
    No dialogflow, clique em "Integrations" e selecione web demo
    Crie um bloco e insira o web demo


