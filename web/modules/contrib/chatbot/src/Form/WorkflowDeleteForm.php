<?php

namespace Drupal\chatbot\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Workflow entities.
 *
 * @ingroup chatbot
 */
class WorkflowDeleteForm extends ContentEntityDeleteForm {


}
