<?php

namespace Drupal\chatbot\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Chatbot entities.
 *
 * @ingroup chatbot
 */
class ChatbotDeleteForm extends ContentEntityDeleteForm {


}
