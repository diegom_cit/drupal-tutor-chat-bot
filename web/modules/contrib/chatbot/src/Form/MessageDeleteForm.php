<?php

namespace Drupal\chatbot\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Message entities.
 *
 * @ingroup chatbot
 */
class MessageDeleteForm extends ContentEntityDeleteForm {


}
