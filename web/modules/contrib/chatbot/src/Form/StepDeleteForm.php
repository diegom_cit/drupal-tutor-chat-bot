<?php

namespace Drupal\chatbot\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Step entities.
 *
 * @ingroup chatbot
 */
class StepDeleteForm extends ContentEntityDeleteForm {


}
