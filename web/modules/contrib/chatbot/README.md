# Chat Framework
Bot Framework for Drupal 8 - Experimental approach to building a framework with a plugable interface for bot clients and back ends. Leveraging Drupal's ability to allow users to easily create and modify content. Based on concepts from https://github.com/WhiteHouse/fb_messenger_bot .
